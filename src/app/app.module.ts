import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser'
import { AppComponent } from './app.component'
import { appRouter } from './app.router';
import { AppService } from './core.service/app.service';
import { LayoutModule } from './layout/layout.module';
import { LoginModule } from './login/login.module';
import { MytestComponent } from './mytest/mytest.component';

@NgModule({
    declarations: [AppComponent, MytestComponent],
    imports: [BrowserModule, LoginModule, appRouter, LayoutModule, HttpClientModule],
    bootstrap: [AppComponent],
  providers:[AppService]
})
export class AppModule { }
