import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MytestComponent } from './mytest.component';

describe("component mytest ok",()=>{

  let component: MytestComponent
  let fixture: ComponentFixture<MytestComponent>
  let tpl: HTMLElement

  beforeEach(async ()=>{
    await TestBed.configureTestingModule({
      declarations:[MytestComponent]
    }).compileComponents() //la compilation est une promise !

    fixture = TestBed.createComponent(MytestComponent)
    fixture.detectChanges()//lance le cycle de détéction de changements
    component = fixture.componentInstance //component

    tpl=fixture.nativeElement
  })

  it('title ok',()=>{
    const h1 = tpl.querySelector('h1') //DOM

    expect(component.title).toBe("Gestionnaire d'utilisateur")
    expect(h1?.textContent).toContain("Gestionnaire d'utilisateur")
  })
})
