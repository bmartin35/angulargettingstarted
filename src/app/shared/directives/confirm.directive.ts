import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Directive({
  selector:'[confirm]'
})
export class ConfirmDirective{
  @Input('confirm') confirmDialog: string='' //la var doit avoir le meme nom 'confirm' que la directive
  @Input() confirmUsername:string=""
  @Output() onConfirm:EventEmitter<void>=new EventEmitter<void>()

  @HostListener('click')//quand je clique sur l'hote, le btn suppr je lance la methode openDialog
  openDialog(){
    if(window.confirm(this.confirmDialog + this.confirmUsername)){
      this.onConfirm.emit()
    }
  }
}
