import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name:'plural'})
export class PluralPipe implements PipeTransform {
  transform(str:string, size:number):string {
    return size > 1 ? str+'s' : str;
  }
}
