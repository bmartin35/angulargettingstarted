import { Pipe, PipeTransform } from '@angular/core';
import { tradEn } from '../trad.en';
import { tradFr } from '../trad.fr';

@Pipe({name:"lang"})
export class LangPipe implements PipeTransform{
  public transform(str:string,lang:string): string {
    switch (lang) {
      case("fr"):
        return tradFr[str];
      break
      case("en"): return tradEn[str];
      break
      default : return "";
    }


  }

}
