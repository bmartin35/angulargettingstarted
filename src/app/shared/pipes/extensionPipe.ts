import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../core.interfaces/user';

@Pipe({name:"extension"})
export class ExtensionPipe implements PipeTransform{
  public transform(users:User[], str:string): User[] {
    if(str==""){
      return users
    }
    return users.filter(user => user.email.endsWith(str))
  }

}
