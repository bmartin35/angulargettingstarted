import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../core.interfaces/user';

@Pipe({name:"nb"})
export class NbPipe implements PipeTransform{
  public transform(users:User[], nb:number): User[] {
    if(nb==100){
      return users
    }
    return users.splice(0,nb)
  }

}
