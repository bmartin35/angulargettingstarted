import { NgModule } from '@angular/core';
import { ConfirmDirective } from './directives/confirm.directive';
import { AutoCompletePipe } from './pipes/AutoCompletePipe';
import { ExtensionPipe } from './pipes/extensionPipe';
import { LangPipe } from './pipes/langPipe';
import { NbPipe } from './pipes/nbPipe';
import { PluralPipe } from './pipes/plural.pipe';

const declarations=[PluralPipe, LangPipe, AutoCompletePipe, ExtensionPipe, NbPipe, ConfirmDirective]

@NgModule({
  declarations:declarations,
  imports:[],
  exports:declarations
})
export class SharedModule{

}
