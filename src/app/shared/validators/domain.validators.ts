import { AbstractControl } from '@angular/forms';

export function domainValidators(domainAllowed: string) {//closure fct de fct
  return function(input:AbstractControl): {domain: string}|null {
    return input.value.endsWith(domainAllowed) ?null:{domain:domainAllowed}
  }
}


