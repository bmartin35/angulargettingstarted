import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent {


  @Output()
  onSubmit: EventEmitter<any> = new EventEmitter<any>()

  createUser(form: NgForm){
    if(form.invalid) return
    this.onSubmit.emit(form)
    form.resetForm()
  }
}
