import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { SearchComponent } from './search/search.component';
import { UserCardComponent } from './user-card/user-card.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UsersComponent } from './users.component';

@NgModule(
  {
    declarations : [UsersComponent, UserCardComponent, UserFormComponent, SearchComponent],
    imports: [FormsModule, CommonModule, SharedModule, RouterModule],
    exports : [UsersComponent]
  }
)
export class UsersModule{

}
