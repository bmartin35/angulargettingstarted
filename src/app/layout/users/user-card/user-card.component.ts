import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../../../core.interfaces/user';

@Component(
  {
    selector:'app-user-card',
    template:`<article>
      <header>{{ user.username }}</header>
      {{ user.email }}<footer>
      <button
        confirm="Etes vous sûr de vouloir supprimer " [confirmUsername]="user.username"
        (onConfirm)="delete()">
        {{ 'REMOVE' | lang:"fr" }}
      </button>
      <button [routerLink]="['user', user.id]">Modifier</button>
    </footer></article>`
  }
)

export class UserCardComponent {
  @Input()
  user: User = {} as User

  @Output()
  onDelete = new EventEmitter

  delete(){
    this.onDelete.emit(this.user.id)
  }
}
