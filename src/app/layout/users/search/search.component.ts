import { Component, EventEmitter, Input, Output } from '@angular/core';
import { UserService } from '../../../core.service/user.service';

@Component(
  {
    selector:'app-search',
    template:`<div style="display: flex; padding: 5%">
      <p style="padding: 1%">Recherche</p>
      <input  type="text" [(ngModel)]="username">
      <button (click)="search()" *ngIf="username">search</button></div>
    <ul><li *ngFor="let username of usernames |autocomplete:username">{{ username }}</li></ul>`
  }
)
export class SearchComponent{
  @Input()
  username=""

  @Output()
  onSearch : EventEmitter<string> = new EventEmitter<string>()

  usernames:string[]=[]

  search(){
    this.onSearch.emit(this.username.toLowerCase())
  }

  constructor(private userService : UserService) {}
  ngOnInit(){
    this.userService.getUsers().subscribe(users=>this.usernames=users.map(user=>user.username.toLowerCase()))
  }
}
