import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../../core.interfaces/user';
import { UserService } from '../../core.service/user.service';

@Component(
  {
    selector:'app-users',
    templateUrl:'users.component.html',
    styleUrls: ['./users.component.css']
  }
)
export class UsersComponent{

  nbSelected = 100

  extension:string=""
  extensions:string[]=['tv','biz','net','org']

  users: User[] = []

  username= ""

  constructor(private userService: UserService) {
    //userService.getUsers().subscribe(users=>this.users=users)
  }

  ngOnInit(){
    this.userService.getUsers().subscribe(res=>this.users=res)
  }

  /* version promise
  async ngOnInit(): Promise<void>{
    this.users = await this.userService.getUsers()
    //this.userService.getUsers().then(users=>this.users=users)
  }
   */

  listenSearch(username: string) {
    console.log(username)
  }

  createUser(form: NgForm){
    this.userService.create({
      email:form.value.email,
      username:form.value.username
    }).subscribe((user: User)=>{
      this.users.push(user);
      this.users=[...this.users]// clone pour pipe
    })
  }

  deleteUser(id:number){
    this.userService.delete(id).subscribe(()=>
      {
        /* attention ne fonctionne pas avec les pipes, pb de références
        let index = this.users.findIndex(user=>user.id==id)
        this.users.splice(index,1)
         */
        this.users = this.users.filter(user=>user.id!=id)
      }
    )

  }
}
