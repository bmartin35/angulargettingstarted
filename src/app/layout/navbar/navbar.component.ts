import { Component } from "@angular/core";
import { AppService } from '../../core.service/app.service';

@Component({
    selector: 'app-navbar',
    template: `
      <div style="display:flex; align-items: center;">
      <h1 style=" width: 100%"><a routerLink="">{{title | uppercase}}</a></h1>
      <button routerLink="/login" style=" width: 30%">Connexion</button>
      </div>
    `
})
export class NavbarComponent {
  title=""
  price=15

  constructor(private appService: AppService) {//initialisations, quand le composant est instancié
  }

  ngOnInit(){//actions, quand le composant est initialisé
    this.title=this.appService.getTitle()
  }


}

