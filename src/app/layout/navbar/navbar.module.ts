import { CommonModule } from '@angular/common';
import { NgModule } from "@angular/core";
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { NavbarComponent } from "./navbar.component";

@NgModule({
    declarations: [NavbarComponent],
    imports: [FormsModule, CommonModule, SharedModule, RouterModule],
    exports: [NavbarComponent]
})
export class NavbarModule {}
