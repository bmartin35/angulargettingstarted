import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { UserEditComponent } from './user-edit.component';

@NgModule({
  declarations: [
    UserEditComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ],
})
export class UserEditModule { }
