import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { User } from '../../core.interfaces/user';
import { UserService } from '../../core.service/user.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent {

  user: User = {} as User

  username: FormControl = new FormControl('',[
    Validators.required
  ])
  form: FormGroup = this.builder.group({
    email: this.user.email,
    username: this.username
  })

  constructor(private route: ActivatedRoute, private userService: UserService, private builder:FormBuilder) {
  }

  ngOnInit(){
    /* équivalent pas propre
    this.route.params.subscribe((params:Params)=>{
        const id = params['id']
        this.userService.getUser(id).subscribe(user=>this.user=user)
      }
    )
     */
    this.route.params.pipe(
      switchMap(params=> {
        const id = params['id']
        return this.userService.getUser(id)
      })
    ).subscribe(user=>{
      this.user=user
      this.form.patchValue({user})
    }

    )
  }

  updateUser(){
    console.log(this.form.value.email)
    this.userService.update(this.user.id, this.form.value).subscribe(user => this.user=user)
  }


}
/*
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { User } from '../../core.interfaces/user';
import { UserService } from '../../core.service/user.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent {

  user: User = {} as User

  username: FormControl = new FormControl('',[
    Validators.required
  ])
  form: FormGroup = this.builder.group({
    email: '',
    username: this.username
  })

  constructor(private route: ActivatedRoute, private userService: UserService, private builder:FormBuilder) {
  }

  ngOnInit(){
    /* équivalent pas propre
    this.route.params.subscribe((params:Params)=>{
        const id = params['id']
        this.userService.getUser(id).subscribe(user=>this.user=user)
      }
    )
     */
/*
this.route.params.pipe(
  switchMap(params=> {
    const id = params['id']
    return this.userService.getUser(id)
  })
).subscribe(
  user=>this.user=user
)
}

updateUser(){
  console.log(this.form.value.email)
  this.userService.update({
    email:this.form.value.email,
    username:this.form.value.username
  })
}


}

 */
