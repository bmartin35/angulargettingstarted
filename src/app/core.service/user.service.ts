import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable,lastValueFrom } from 'rxjs';
import { User } from '../core.interfaces/user';

type UserPayload = Omit<User, 'id'>//interface qui rpz un user sans id

@Injectable(
  {
    providedIn:'root' //AppModule n'a plus besoin de connaitre les services
  }
)
export class UserService{
  readonly url: string = 'https://jsonplaceholder.typicode.com/users'

  constructor(private httpClient:HttpClient) {
  }

  /* version promise
  getUsers(): Promise<User[]>{
    return new Promise<User[]>((resolve, reject)=>
      this.httpClient.get<User[]>(this.url).subscribe(
        res=> resolve(res)));
    //return lastValueFrom(this.httpClient.get<User[]>(this.url))
  }
   */
  getUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>(this.url)
  }

  getUser(id:number): Observable<User> {
    return this.httpClient.get<User>(this.url+"/"+id)
  }

  create(userPayload:UserPayload): Observable<User>{
    return this.httpClient.post<User>(this.url, userPayload)
  }

  update(id:number, userPayload:UserPayload): Observable<User>{
    return this.httpClient.put<User>(this.url+"/"+id, userPayload)
  }

  delete(id:number): Observable<any>{
    return this.httpClient.delete<boolean>(this.url+"/"+id)
  }
}
