import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { domainValidators } from '../shared/validators/domain.validators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  propEmail: FormControl = new FormControl('',[
    Validators.required,
    Validators.minLength(4),
    domainValidators("toto.fr")
  ])
  propPassword: FormControl = new FormControl('',[
    Validators.required
  ])

  form: FormGroup = this.builder.group({
    email: this.propEmail,
    password: this.propPassword
  })

  submitted=false

  constructor(private builder:FormBuilder) {
  }

  login(){
    console.log(this.form.value)
    this.submitted=true
  }
}
