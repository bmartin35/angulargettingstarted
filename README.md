# Context
Project developed during Angular formation in 3 days.

## Homepage :
![img.png](screen/home.png)

## Login :
![img.png](screen/login.png)

## Edit :
![img.png](screen/edit.png)

## Requirements
Angular version 15

Node version 18

Npm version 8

Terminal

## Launch local serv
npm i

ng serve 

go to http://localhost:4200
